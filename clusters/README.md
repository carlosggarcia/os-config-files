# Specific notes for cluster user config files

- zsh: In my experience, one ends up having to install zsh from scratch and add `.zshenv` to avoid this error

    ```
    /etc/zshrc:71: compinit: function definition file not found
    prompt: command not foun
    ```
