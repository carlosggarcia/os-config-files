#! /bin/sh

check_battery(){
	level_dead=15
	level_crit=20
	level_warn=25

	battery=$(cat /sys/class/power_supply/BAT0/capacity)
	remaining=$(acpi -b | awk '{ print $5; }')
	if [ "$battery" -lt $level_dead ]; then
		message="Battery lower than ${level_dead}%: ${battery}%!!!. Suspending in 3s"
		notify-send "$message" -u critical
		sleep 3
		systemctl suspend
	elif [ "$battery" -lt $level_crit ]; then
		message="Battery lower than ${level_crit}%: ${battery}%. Remaining time: $remaining."
		notify-send "$message" -u critical
	elif [ "$battery" -lt $level_warn ]; then
		message="Battery lower than ${level_warn}%: ${battery}%. Remaining time: $remaining."
		notify-send "$message"
	fi
}


bat_status=$(cat /sys/class/power_supply/BAT0/status)
if [ "$bat_status" == "Discharging" ]; then
    check_battery
fi
