#!/bin/bash
# /etc/acpi/handler.sh
# Default acpi script that takes an entry for all actions

case "$1" in
    button/power)
        case "$2" in
            PBTN|PWRF)
                logger 'PowerButton pressed'
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
    button/sleep)
        case "$2" in
            SLPB|SBTN)
                logger 'SleepButton pressed'
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
    ac_adapter)
        case "$2" in
            AC|ACAD|ADP0)
                case "$4" in
                    00000000)
                        logger 'AC unpluged'
                        ;;
                    00000001)
                        logger 'AC pluged'
                        ;;
                esac
                ;;
            *)
                logger "ACPI action undefined: $2"
                ;;
        esac
        ;;
    battery)
        case "$2" in
            BAT0)
                case "$4" in
                    00000000)
                        logger 'Battery online'
                        ;;
                    00000001)
                        logger 'Battery offline'
                        ;;
                esac
                ;;
            CPU0)
                ;;
            *)  logger "ACPI action undefined: $2" ;;
        esac
        ;;
    button/lid)
        case "$3" in
            close)
                logger 'LID closed'
		# Done by hardware!!
		# sudo -u ardok /usr/bin/slock systemctl suspend -i
		# systemctl suspend
                ;;
            open)
                logger 'LID opened'
		# Done by hardware!!
		# /usr/bin/light -S 30
                ;;
            *)
                logger "ACPI action undefined: $3"
                ;;
    
        esac
        ;;

    # Manged by X11 as pulseaudio is recomended to be run as a user daemon
    # 
    # button/mute) /usr/bin/amixer -q set Master toggle ;;
    # button/volumeup) /usr/bin/amixer -q set Master 2dB+ unmute ;;
    # button/volumedown) /usr/bin/amixer -q set Master 2dB- unmute ;;
    
    # Using light instead of xbacklight because it didn't work
    video/brightnessup) /usr/bin/light -A 5 ;; 
    video/brightnessdown) /usr/bin/light -U 5 ;;


    *)
        logger "ACPI group/action undefined: $1 / $2"
        ;;
esac

# vim:set ts=4 sw=4 ft=sh et:
