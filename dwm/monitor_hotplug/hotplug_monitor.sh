#! /usr/bin/bash

export DISPLAY=:0
export XAUTHORITY=$HOME/.Xauthority

laptop_screen=eDP-1
disconnected_screens=`xrandr | grep " disconnected " | awk '{ print$1 }'`
connected_screens=`xrandr | grep " connected " | grep -v $laptop_screen |  awk '{ print$1 }'`

ncs=`wc -w <<< $connected_screens`

for ds in $disconnected_screens; do
    xrandr --output $ds --off
done

if [ `wc -w <<< $connected_screens` -eq 0 ]; then
    xrandr --output $laptop_screen --auto
else
    for cs in $connected_screens; do
        xrandr --output $cs --right-of $laptop_screen --auto
    done
fi
