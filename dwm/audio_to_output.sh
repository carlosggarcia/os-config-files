#! /usr/bin/bash
# Script to switch to differnt audio ouputs

if [[ "$1" == "HDMI-1" || "$1" == "HDMI-laptop" ]]; then
    pactl set-card-profile 0 output:hdmi-stereo-extra1+input:analog-stereo
elif [[ "$1" == "HDMI-3" || "$1" == "HDMI-dock" ]]; then
    pactl set-card-profile 0 output:hdmi-stereo-extra3+input:analog-stereo
elif [ "$1" == "laptop" ]; then
    pactl set-card-profile 0 output:analog-stereo+input:analog-stereo
else
    echo "Output not recognized. Availables are: [HDMI-1 or HDMI-laptop, HDMI-3 or HDMI-dock, laptop]"
    echo "If you need to run the command by hand find the correct output with:"
    echo "    pactl list cards | grep output"
    echo "    pactl set-card-profile 0 output:XXX"

fi
