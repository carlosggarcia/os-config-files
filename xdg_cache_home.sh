#/etc/profile.d/xdg_cache_home.sh
# From: https://wiki.gentoo.org/wiki/SSD
if [ $USER ]; then
  export XDG_CACHE_HOME="/tmp/${USER}/.cache"
fi
