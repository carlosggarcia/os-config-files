#!/bin/sh

set -e

backlight_sys_dir="/sys/class/backlight/intel_backlight"

read -r max_brightness < "${backlight_sys_dir}/max_brightness"
read -r curr_brightness < "${backlight_sys_dir}/brightness"

case "$1" in
   up) increment="+375" ;;
   down) increment="-375" ;;
   *) exit 1 ;;
esac

new_brightness=$(($curr_brightness $increment))

if (($new_brightness < 1)) || (($new_brightness > $max_brightness)); then
   exit 1
else
   echo "$new_brightness" > ${backlight_sys_dir}/brightness
fi 
