" --------------------------------
"  My config
" --------------------------------
"

if has('mouse')
    set mouse=a
endif

set foldmethod=indent

set tw=79
" Color column with textwidth + 1
set colorcolumn=+1

autocmd FileType tex,bib
       \ call deoplete#custom#buffer_option('auto_complete', v:false)

" Airline
let g:airline#extensions#tabline#enabled = 1

" pydocstring
let g:pydocstring_formatter = 'google'

" Codeium disable dy default
let g:codeium_enabled = v:false

" --------------------------------
"  End of my config
" --------------------------------
