`config.vim` is a modified version of [fisa-vim-config](https://github.com/fisadev/fisa-vim-config)'s one

Link `~/.conf/nvim/init.vim` and `~/.vimrc` to `config.vim`
Link `custom.vim` and `after/ftplugin/tex.vim` to their respective files in `~/.conf/nvim/` and `~/.vim/`
