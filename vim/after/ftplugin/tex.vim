"Latex specific configuration

" General
setlocal nocursorline
setlocal keymap=
setlocal spell spelllang=en_us

set sw=2
set tw=78
set iskeyword+=:

" Stop autopairing in tex files " ` "
let g:AutoPairs={'(':')', '[':']', '{':'}',"'":"'",'"':'"'}


" vimtex
let g:vimtex_view_method = "zathura"
let g:vimtex_view_general_viewer = 'zathura'


" vim-latex

" set grepprg=grep\ -nH\ $* "From README.Debian for latex-suite
" let g:tex_flavor='latex'  "Same as above
" let g:Tex_DefaultTargetFormat = 'pdf'
" let g:Tex_MultipleCompileFormats='pdf, aux'"Latex specific setting
" let g:Tex_ViewRule_pdf = 'zathura'
" TTarget pdf

" Stop automatic jumping to warning location after compilation
" let g:Tex_GotoError=0

" Supress warning for hyperref
" let g:Tex_IgnoredWarnings = 'Package hyperref Warning'."\n"

" Stop autocompletion
" autocmd FileType tex
"       \ call deoplete#custom#buffer_option('auto_complete', v:false)
