# OS config files

Repository with OS configuration files that I use among different computers.

Nothing fancy.

To install the packages in arch-pkglist.txt do: `pacman -S - < arch-pkglist.txt`

Useful notes:

* zoom with ALSA: In `~/.config/zoomus.conf` change `system.audio.type=default` to `system.audio.type=alsa` (19/03/2021)
* vim/nvim: follow https://vim.fisadev.com/
* Pulseaudio: moved to Pulseaudio for easier handling of different outputs/sources devices (e.g. HDMI)
* Jupyter extension:
  - Install:
    `pip install --user jupyter_contrib_nbextensions jupyter_nbextensions_configurator`
  - Enable them:
    `jupyter nbextensions_configurator enable --user`
    `jupyter contrib nbextension install --user`
* `iwcd` has its own DHCP but it's better to have `dhcpcd` installed in order
    to be able to configure Ethernet connections.
* `pyccl` needs `pacman -S cmake gsl swig`
* Update Arch package lists:
  - `pacman -Qqen > arch-pkglist.txt`
  - `pacman -Qqem > arch-AUR-pkglist.txt`
* Bluetooth:
  - Start service: `systemctl start bluetooth`
  - Power on and pair with `bluetoothctl` (and connect if needed)
  - Send files with `obexctl` (with `connect` and `send`) after starting its
      daemon `systemctl start obex --user`
  - `pulseaudio-bluetooth` needed for headset
* LightDM:
  - `dm-tool` let you switch users and even open a new user desktop in your own with `add-nested-seat`. It requires `xorg-server-xephyr`
* DHCP connection when filtered by MAC: add the following lines in the
    configuration files of `systemd-networkd` (i.e. in `/etc/systemd/network`)
 ```
 [DHCPv4]
 ClientIdentifier=mac
 ```
* Using `systemd-networkd`: enable `systemd-networkd.service` and `systemd-resolved.service`
